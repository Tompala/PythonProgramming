arr = []
for i in range(5):
    row = []
    for j in range(5):
        row.append(0)
    arr.append(row)
    
    arr = [[0 for _ in range(5)] for _ in range(5)]
    
    arr = [[0] * 5 for _ in range(5)]

# -------------------------------------------------------------------------

cukkiniUltetveny = [
    [3, 1, 4, 1, 5],
    [9, 2, 6, 5, 3],
    [5, 8, 9, 7, 9],
    [3, 2, 3, 8, 4],
    [6, 2, 6, 4, 3]
]

sumCukkini = 0
# Elso fajta (pythonban ez a szep) megoldas
for row in cukkiniUltetveny:
    for cukkini in row:
        sumCukkini += cukkini
     
print(f"Cukkinik teljes szama: {sumCukkini}")
   
sumCukkini = 0
# Masodik fajta megoldas        
for i in range(len(cukkiniUltetveny)):
    for j in range(len(cukkiniUltetveny[i])):
        sumCukkini += cukkiniUltetveny[i][j]

print(f"Cukkinik teljes szama: {sumCukkini}")

# -------------------------------------------------------------------------
# Parameter es visszateresi ertek nelkuli fuggveny
def PrintALine():
    print("--------------------------------------------------------")

PrintALine()

# Parameterekkel rendelkezo fuggveny
def PrintGreeting(name):
    print(f"Hello {name}!")
    
PrintGreeting("Lujcsi")

# Parameterekkel es visszateresi ertekkel rendelkezo fuggveny
def HaromszogTerulete(oldal, magassag):
    T = (oldal * magassag) / 2
    return T

Terulet = HaromszogTerulete(6, 4)
print(f"A haromszog terulete {Terulet}cm^2")

# Tobb visszateresi ertek is lehetseges
def MilyenMesszireJutokMennyiIdoAlatt(fogyasztas, uzemanyag, sebesseg):
    tavolsag = (uzemanyag / fogyasztas) * 100
    ido = tavolsag / sebesseg
    return tavolsag, ido

fogyi = 5.6
tank = 60
sebesseg = 110
tav, ido = MilyenMesszireJutokMennyiIdoAlatt(fogyi, tank, sebesseg)
print(f"Ha {sebesseg} km/h-val megyek, {fogyi}l/100km a fogyasztasom es {tank}l van a tankomban, akkor {tav}km-re jutok el {ido} ora alatt")

def PrintRamdomThings(first="Hello", second="Lujcsi", third="Gyongyoson"):
    print(f"{first} {second}! Mi folyik itt {third}?")

PrintRamdomThings()
# output: Hello Lujcsi! Mi folyik itt Gyongyoson?
PrintRamdomThings(second="Fittyes")
# output: Hello Fittyes! Mi folyik itt Gyongyoson?
PrintRamdomThings(first="Mi van", third="Mateszalkan")
# output: Mi van Lujcsi! Mi folyik itt Mateszalkan?
PrintRamdomThings("Stajni", "Majni", "Nyamvadtfalvan")
# output: Stajni Majni! Mi folyik itt Nyamvadtfalvan?

def main():
    # Minden utasitast ide irjunk
    asd = 0

if __name__ == "__main__":
    main()