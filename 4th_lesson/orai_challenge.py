def main():
    idojaras2010_2023 = [
        [-3, 1, 10, 14, 12, 16, 25, 20, 19, 15, 7, 1],    # 2010
        [-4, -1, 1, 8, 19, 20, 19, 27, 21, 11, 2, 0],     # 2011
        [0, 3, 5, 10, 14, 25, 24, 23, 12, 12, 3, -1],     # 2012
        [1, 0, 1, 10, 18, 16, 19, 19, 20, 10, 2, -2],     # 2013
        [-2, -3, 7, 9, 14, 22, 26, 17, 16, 14, 2, 2],     # 2014
        [0, 3, 4, 16, 20, 23, 28, 24, 21, 10, 4, -1],     # 2015
        [0, -3, 8, 10, 13, 18, 28, 21, 20, 14, 7, 0],     # 2016
        [-1, -1, 10, 7, 16, 20, 22, 22, 17, 13, 2, 2],    # 2017
        [1, 2, 8, 14, 15, 22, 24, 24, 15, 9, 7, 2],       # 2018
        [-3, -2, 3, 8, 16, 21, 22, 19, 18, 7, 4, 0],      # 2019
        [-3, -2, 8, 15, 15, 25, 22, 25, 19, 12, 2, 0],    # 2020
        [-1, 1, 6, 13, 18, 20, 20, 27, 22, 7, 8, 0],      # 2021
        [-4, 3, 8, 12, 16, 16, 21, 19, 22, 11, 7, 0],     # 2022
        [-2, -3, 9, 8, 21, 24, 22, 19, 20, 13, 3, 1]      # 2023
    ]
    
    # Nem szukseges de egy folytonosabban hasznalhato program elmenyert egy vegtelen ciklusba teszem az egeszet
    while True:
        print("Add meg milyen honap van, es hogy melyik evek alapjan szeretnel idojaras elorejelzest.")
        honap = int(input("Hanyadik honap van? Ha 0-t adsz meg, a program kilep: ")) - 1
        # ha a honapnal 0-t adunk meg akkor az erteke -1 (mert kivonunk belole egyet). Ekkor a program kilep.
        # A program kilep akkor is ha a honap negativ, vagy nagyobb vagy egyenlo mint 12, ebben az esetben ugyanis errorra
        # futnank, a megadott tomb sorainak nincs 12-es indexu vagy annal nagyobb eleme
        if (honap <= -1) or (honap >= 12):
            print("A program kilep!")
            break
        # Az eveket vesszovel elvalasztva kerem be, igy egy tombot csinalok a beolvasott szovegbol azonnal a 
        # .split(',') metodussal
        evek = input("Melyik evek alapjan mondjak elorejelzest? Add meg oket vesszovel elvalasztva: ").split(',')
        # Szamokra van szuksegem igy a tomb minden elemet integerre castolom. a .strip() metodus elotte levag minden
        # szokozt a string elejerol es vegerol, igy ha valaki igy adna meg: "2010, 2011, 2013" akkor is mukodik a program
        # Az enumerate()-el letrehozott for ciklust hasznalom, mert az illik a legjobban a helyzethez. Nem akarom masik
        # tombbe felvenni a castolt szamokat, igy az index hasznalataval vissza tudom irni a kapott ertekeket az eredeti
        # tombbe, effektive atkonvertalva minden elemet integerre
        for idx, ev in enumerate(evek):
            evek[idx] = int(ev.strip())
        
        # A bonyolultabb megoldast alkalmazva csak a tablazatot, a honapot es az evek tombot adom at a fuggvenynek
        elorejelzettHomerseklet = elorejelzesKalkulalasBonyibb(idojaras2010_2023, honap, evek)
        print(f"Az elorejelzett homerseklet erre a honapra: {elorejelzettHomerseklet} Celsius fok")
        print("")
        
        # Hogy a tombos beolvasasi modszeremmel is mukodjon a konnyebb megoldas ha tobb, mint 2 evet adunk meg, csak az
        # elso kettot szamitjuk ebben az esetben
        elorejelzettHomerseklet = elorejelzesKalkulalasKonnyebb(idojaras2010_2023, honap, evek[0], evek[1])
        print(f"Az elorejelzett homerseklet erre a honapra: {elorejelzettHomerseklet} Celsius fok")
        print("")
        
def elorejelzesKalkulalasBonyibb(tabla, ho, evek):
    # Az elorejelzes valtozoban fogom osszeadni az adott honapban talalt homerseklet ertekeket
    elorejelzes = 0
    # Melyet ezzel a for ciklussal csinalok.
    for ev in evek:
        # Ha a megadott evbol kivonok 2010-et akkor pont a megfelelo sor indexet kapom
        # A honap pedig pont az adott sor megfelelo elemere mutat hiszen a beolvasasnal mar kivontam belole egyet
        elorejelzes += tabla[ev-2010][ho]
    # A visszateresi erteknel az elorejelzes valtozot elosztjuk az evek tomb hosszaval, igy kapunk atlagot
    return elorejelzes / len(evek)

def elorejelzesKalkulalasKonnyebb(tabla, ho, ev1, ev2):
    # Egy sorban kiszamoljuk a megadott ket ev adott honapjainak atlagat es visszaterunk vele
    return (tabla[ev1-2010][ho] + tabla[ev2-2010][ho]) / 2
        

if __name__ == "__main__":
    main()