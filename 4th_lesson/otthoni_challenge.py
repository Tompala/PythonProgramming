def main():
    while True:
        szamok = input("Add meg a szamokat ','-kel elvalasztva: ").split(',')
        if len(szamok) == 1:
            print("Egy szam keves lesz, kilep a program.")
            break
        for idx, szam in enumerate(szamok):
            szamok[idx] = int(szam.strip())
        
        valasztas = int(input("Osszeadni szeretned a szamokat(0) vagy osszeszorozni?(1): "))
        if valasztas:
            eredmeny = szorzas(szamok)
        else:
            eredmeny = osszeadas(szamok)
            
        print(f"Az eredmeny: {eredmeny}")
        print("")
            
def szorzas(szamok):
    eredmeny = 1
    for szam in szamok:
        eredmeny *= szam
    return eredmeny

def osszeadas(szamok):
    eredmeny = 0
    for szam in szamok:
        eredmeny += szam
    return eredmeny

if __name__ == "__main__":
    main()
    