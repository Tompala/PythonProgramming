# input
number_string = input("Enter numbers separated by commas: ")
# Tombbe konvertalas
number_list = number_string.split(',')
# szamlalok inicializalasa
even_count = 0
odd_count = 0
# A tomb minden elemen vegigiteralunk es eldonjuk hogy paros vagy paratlen
# miutan integer-e alakitottuk. Valamint noveljuk a szamlalot egyel
for number_str in number_list:
    number = int(number_str)
    if number % 2 == 0:
        print(f"{number} is even")
        even_count += 1
    else:
        print(f"{number} is odd")
        odd_count += 1

# Print out the total counts
print(f"Total even numbers: {even_count}")
print(f"Total odd numbers: {odd_count}")