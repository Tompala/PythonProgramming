
import math
# While ciklus pelda ahol ki akarjuk prointelni az elso 20 primszamot egy szam felett
# Primszam az ami csak magaval es az 1-el oszthato
primCounter = 0
szam = int(input("Add meg melyik szam felett keressuk a primeket: "))
primek = []
# amig nem gyult ossze a 20 prim addig cilusozunk
while not primCounter == 20:
    # Flag ami azt fogja jelezni hogy talaltunk osztot ami nem az 1 vagy a szam sajat maga
    vanMegOsztojaFlag = False
    # minden szamnal vegigiteralunk a szamokon 2-tol sajat maga feleig. A range() fuggveny ket bemenettel
    # egy tombot general melynek elso eleme az elso helyen megadott szam utolso eleme pedig a masodik helyen megadott -1
    # A range() fuggveny csak itegerekkel mukodik ezert az egesz osztast alkalmazzuk.
    if not (szam % 2 == 0):
        for oszto in range(3, int(math.sqrt(szam)) + 1, 2):
            # Ha az osztoval torteno osztasnal nincs maradek akkor az egy valos osztoja a szamnak
            if szam % oszto == 0:
                vanMegOsztojaFlag = True
                break
        # Ha nem talaltunk osztot akkor ez eg yprimszam. Adjuk a tombhoz es a szamlalot noveljuk egyel
        if not vanMegOsztojaFlag:
            primek.append(szam)
            primCounter += 1
    # A vegen a szamukat noveljuk egyel es futhat ujra a ciklus
    szam += 1

print(primek)
