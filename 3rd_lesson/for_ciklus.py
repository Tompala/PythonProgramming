kedvesTombom = ["alma", "korte", "dinnye", "narancs", "pomelo"]
for element in kedvesTombom:
    print(element)
# Az element valtozo minden cilusban felveszi a tomb kovetkezo elemet es aztan kiprinteli.
# Az element valtozot nem szukseges elore definialnunk, az letrejon ilyenkor
print("")

tomb1 = [1, 2, 3, 4, 5]
tomb2 = [10, 20, 30, 40, 50]
# Tegyuk fel hogy az elso tomb elemeivel kell beszorozni a masodik tomb 
# elemeit es az eredmenyt eltarolni egy uj tombben.
eredmeny = []
for idx in range(len(tomb1)):
    tmp = tomb1[idx] * tomb2[idx]
    eredmeny.append(tmp)
# Az eredmeny tartalma [10, 40, 90, 160, 250]
print(eredmeny)
print("")

tomb1 = [1, 2, 3, 4, 5]
tomb2 = [10, 20, 30, 40, 50]
eredmeny = []
for idx, elem in enumerate(tomb1):
    eredmeny.append(elem * tomb2[idx])
print(eredmeny)

