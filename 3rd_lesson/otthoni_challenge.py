# Megoldas a mostani tudasunkkal, 2 egymasba agyazott for ciklusal
necessary_items = ["paradicsom", "kolbász", "hagyma", "tojás", "kenyér", "tej", "só"]
current_items = ["hagyma", "tej", "só"]

print("Bevásárlólista:")
# Vegigiteralunk a szukseges dolgok listajan
for item1 in necessary_items:
    # Flaget csinalunk arra hogy a szukseges itemet megtalaltuk-e az otthoniak listajaban
    item_found = False
    # Vegigiteralunk most az otthoni itemeken
    for item2 in current_items:
        # Ha talalunk olyan itemet otthon ami az aktualis szuksegessle megegyezik akkor
        # annak az itemnek nem kell felkerulnie a bevasarlolistara
        if item1 == item2:
            item_found = True
    # Ha nem talaltuk meg akkor viszont fel kell hogy keruljon ezert kiirjuk
    if not item_found:
        print(f"- {item1}")
        
# ----------------------------------------------------------------------------------
print("")
# ----------------------------------------------------------------------------------
# Gyorsabb megoldas az "ïn" tombokre alkalmazhato operatorral 
necessary_items = ["paradicsom", "kolbász", "hagyma", "tojás", "kenyér", "tej", "só"]
current_items = ["hagyma", "tej", "só"]

print("Bevásárlólista:")

for item in necessary_items:
    # Az "if item not in current_items" kifejezes pontosan azt jelenti amit angolul jelent.
    # Ha az item nem resze a current_items tombnek akkor ez igaz es kiprinteljuk ezt az itemet
    if item not in current_items:
        print(f"- {item}")
        
        