kedvesTombom = [1, 2, 3, 4, 5]
kedvesTombom.append(6) # Hozzaadok egy elemet a tomb vegere
# A tomb tartalma most [1, 2, 3, 4, 5, 6]

# A tomb valamelyik elemet a tomb indexeinek hasznalataval szerezhetjuk meg.
# Az index azt mondja meg hogy hanyadik elemrol beszelunk a tombben.
# A szamozas nem 1-tol hanem 0-tol indul, igy egy 5 elemes tombben az 
# utolso elem a 4-es indexu elem
tombomElsoEleme = kedvesTombom[0]
tombomMasodikEleme = kedvesTombom[1]
tombomUtolsoEleme = kedvesTombom[-1]
tombomUtolsoElottiEleme = kedvesTombom[-2]

# A tomb valamelyik elemet hasonlokeppen tudjuk modositani
kedvesTombom[2] = 12
# A tomb tartalma most [1, 2, 12, 4, 5, 6] a harmadik elem (melynek indexe 2) modosult

# Tomb inicializalas. Keszitek egy 100 elemu tombot melynek minden eleme 0
szazElemuTomb = [0] * 100

ketdimenziosTomb = [
    [1, 2, 3]
    [4, 5, 6]
    [7, 8, 9]
]

asd = ketdimenziosTomb[1][2]
# asd erteke 6
