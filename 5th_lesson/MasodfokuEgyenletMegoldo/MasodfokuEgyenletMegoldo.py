import math

def main():
    # Megoldjuk a kovetkezo tipusu masodfoku egyenletet:
    # ax^2+bx+c = 0
    print("Ez a program meg fogja oldani a kovetkezo tipusu masodfoku egyenletet: ax^2+bx+c = 0")
    while True:
        try:
            a = float(input("Add meg az 'a' erteket: "))
            b = float(input("Add meg az 'b' erteket: "))
            c = float(input("Add meg az 'c' erteket: "))
            break
        except ValueError:
            print("Majom vagy, mert ez nem egy szam! Kezdd elorol az a,b,c ertekek megadasat!")
    
    # kiszamoljuk a diszkriminanst
    D = disdkriminansSzamito(a, b, c)
    # 3 fajta masodfoku egyenlet letezik. Aminek 2 gyoke van, aminek 1 es aminek nincs gyoke
    if D < 0:
        print("Ennek a masodfoku egyenletnek nincs megoldasa a valos szamok halmazan!")
    elif D == 0:
        x = -b / (2*a)
        print(f"A masodfoku egyenletnek egy megoldasa van van es ez a {x}")
    else:
        x1 = (-b + math.sqrt(b**2 - 4*a*c)) / 2*a
        x2 = (-b - math.sqrt(b**2 - 4*a*c)) / 2*a
        print(f"A masodfoku egyenletnek ket megoldasa van es ezek x1= {x1}, x2= {x2}")
    

def disdkriminansSzamito(a, b, c):
    D = b**2 - 4*a*c
    return D

if __name__ == "__main__":
    main()