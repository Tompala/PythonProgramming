'''
A feladat:
Zoltika szeretne csinalni egy ablakkeretet, azonban ehhez az ablakkerethez mindossze 5m keretanyaga
all rendelkezesre. Azt tudja hogy az ablakkeret alakja egy oldalan allo teglalab a tetejen egy felkorrel.
Az ablaknak mindenkeppen ilyen alakunak kell lennie de az aranyai szabadon megvalaszthatoak.
Zoltika jo ablakkeret keszito modjara a vasarloinak azt szeretne hogy az altala krealt ablak a leheto
legtobb fenyt juttassa majd be a lakasba.
Mekkorara valassza Zoltika az ablak szelesseget hogy a leheto legtobb fenyt engedje az be. A problemat
oldjuk meg numerikus iteracios modszerrel.
'''
import math

def main():
    teljesKerethossz = 4
    szelesseg = 1
    alaplepes = 0.01
    pontossag = 0.00000000000000000000000000000001
    if forditsunkLepesiranyt(szelesseg, alaplepes, teljesKerethossz):
        alaplepes = -alaplepes
        
    while True:
        Ter1 = terulet(szelesseg, teljesKerethossz)
        Ter2 = terulet(szelesseg + alaplepes, teljesKerethossz)
        if Ter2 > Ter1:
            szelesseg = szelesseg + alaplepes
        else:
            alaplepes = alaplepes / 10
            if abs(alaplepes) < pontossag:
                print(f"Az optimalis szelesseg: {szelesseg} m")
                break
            if forditsunkLepesiranyt(szelesseg, alaplepes, teljesKerethossz):
                alaplepes = -alaplepes
    
    
def terulet(a, K):
    return a * ((K - a - (a/2)*math.pi) / 2) + ((((a/2)**2) * math.pi) / 2)

def forditsunkLepesiranyt(szelesseg, alaplepes, K):
    # True ha pozitiv iranyba, False ha negativ iranyba
    Tpozi = terulet(szelesseg + alaplepes, K)
    Tnega = terulet(szelesseg - alaplepes, K)
    return not Tpozi > Tnega

if __name__ == "__main__":
    main()