import External1
import External2

def main():
    External1.PrintMyself()
    External2.PrintMyself()

if __name__ == "__main__":
    main()