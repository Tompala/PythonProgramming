# A profi megoldast a teljes modularitas es ujrafelhasznalhatosag szellemisegeben alkotjuk meg.
# Ez a gondolkodasmod arra vezet minket hogy egy olyan programot alkossunk aminek a bemenete altalanos
# kinezetu, es a kert feladatot meg tudna csinalni barmilyen tombok flehasznalasaval, illetve akarhany
# masodlagos sorbarendezesi feltetellel. Igy ha a feladatunk azt mondana, hogy a versenyzok magassaga alapjan dontjuk
# el ki a jobb ha meg a vizbemerulesuk is megegyezik a pontszam mellett, a programunk ezt is le fogja tudni kezelni
# modositas nelkul, csak az alapadatokat kell megvaltoztatnunk.

# A cel itt tehat az, hogy egy "blackBox" jellegu modult hozzunk letre ami az ilyen falyta sorbarendezeseket
# el tudja vegezni akkor is ha a fletetelek masok
def main():
    Nevek   = ['Jonas', 'Alex', 'Lajos', 'Geza', 'Hedvig', 'Ferenc', 'Margit', 'Lujza', 'Fityma', 'Stajn']
    Pontok  = [88,       88,     65,      82,     73,       79,       74,       88,      82,       78    ]
    Melyseg = [2.4177,   3.3651, 3.1516,  2.7217, 3.2465,   2.1788,   2.4110,   2.3872,  2.2805,   1.8338]
    # A modularitashoz eloszor is a tombokbol egy tablazatot alkotunk. Az nem lenne megfelelo gondolkodasmod, hogy
    # ezt a 3 tombot tegyuk bele egy nagyobba, mert nekunk az lenne a fontos hogy az egymashoz tartozo elemek legyenek
    # egy "sorban" ami a nagy tombunkon belul egy kisebb tomb lesz. Emiatt egy for ciklussal letrehozunk egy uj tombot
    # amihez hozzadogatjuk egy-egy elemkepen mind3 tomb elso, masodik, harmadik... tagjat kulon kis tombben. Igy mar az
    # uj 2D tombunkben 1 sorban valoban az osszetartozo adatok (Nev, Pont, Melyseg) szerepelnek.
    #
    # Belatjuk, hogy ez nekunk nagyon jo, hiszen igy nem kell majd 3 tombben cserelgetni az elemeket hanem eleg lesz a 
    # nagy tablazatban teljes sorokat felcserelni.
    Tablazat = []
    for i in range(len(Nevek)):
        Tablazat.append([Nevek[i], Pontok[i], Melyseg[i]])
    # Meghivjuk a BubbleSort fuggvenyt
    Tablazat = BubbleSort(Tablazat, [1, 2])
    for i in range(len(Tablazat)):
        for j in range(len(Tablazat[i])):
            print(f"{Tablazat[i][j]} \t", end="")
        print("")

def BubbleSort(table, orderOfSorting):
    # ===============================================================================================================
    # DESCRIPTION
    #   Ez a fuggveny sorbarendezest fog vegezni a kovetkezo modon. Egy 2D tablazatot var el bemenetkent, illetve egy
    #   1D tombot amiben a tablazat azon oszlopainak szamai szerepelnek (index szam) amelyek alapjan szeretnenk a 
    #   sorbarendezest vegezni. Ha orderOfSorting erteke a kovetkezo: [1, 2] akkor eloszor a masodik oszlop alapjan 
    #   rendezunk, es ha ez nem tette egyertelmuve a sorrendet akkor utana a 3. oszlop alapjan
    # ===============================================================================================================
    # Egy for ciklussal vegig megyunk az oszlopokat tartalmazo tombon. Ez utan pedig sima BubbleSort-ot vegzunk el 
    # ugy, hogy odafigyelunk, mi volt a sorbarendezes az elozo oszlop alapjan ha volt ilyen
    for i in range(len(orderOfSorting)):
        while True:
            csere = 0
            for j in range(len(table) - 1):
                # A lastEqual flag fogja jelezni nekunk, hogy az elozo sorbarendezes alapjan a vizsgalt 2 elem egyenlo
                # volt-e. Ezt alapvetoen False-ra allitjuk.
                lastEqual = False
                # Ha a legelso sorbarendezest vegezzuk akkor nem volt elozo oszlop, igy itt 2 fele kellene agaznunk a 
                # programmal ami bonyolultabba tenne a kotod. Elkerulheto ez azzal, ha egyszeruen csak odakepzelunk egy
                # elozo oszlopot ami alapjan mindegyik sor egyenlo volt.
                if i == 0:
                    lastEqual = True
                # Ha nem a legelso sorbarendezest vegezzuk akkor megvizsgaljuk, hogy az elozo sorbarendezes alapjan a 
                # most vizsgalt 2 elem azonos volt-e.
                elif table[j+1][orderOfSorting[i-1]] == table[j][orderOfSorting[i-1]]:
                    lastEqual = True
                # Abban az esetben amennyiben a most vizsgalt 2 elem kozul a hatrabb levo a nagyobb ES az elozo 
                # vizsgalat alapjan egyenloek voltak, cseret vegzunk el
                if table[j+1][orderOfSorting[i]] > table[j][orderOfSorting[i]] and lastEqual:
                    table = ElemCsere(table, j, j+1)
                    csere += 1
            if csere == 0:
                break
            
    return table
    
def ElemCsere(T, i, j):
    tmp = T[i]
    T[i] = T[j]
    T[j] = tmp
    return T

if __name__ == "__main__":
    main()
