def main():
    Nevek   = ['Jonas', 'Alex', 'Lajos', 'Geza', 'Hedvig', 'Ferenc', 'Margit', 'Lujza', 'Fityma', 'Stajn']
    Pontok  = [88,       88,     65,      82,     73,       79,       74,       88,      82,       78    ]
    Melyseg = [2.4177,   3.3651, 3.1516,  2.7217, 3.2465,   2.1788,   2.4110,   2.3872,  2.2805,   1.8338]
    
    # Elvegezzuk a BubbleSort cserelgetest a pontszamok alapjan
    while True:
        csere = 0
        for i in range(len(Nevek) - 1):
            if Pontok[i+1] > Pontok[i]:
                # Mind3 tablazatba elvegezzuk a cseret hopgy az osszetartozo ertekek tovabbra is egyben maradjanak
                tmp = Nevek[i] 
                Nevek[i] = Nevek[i+1]
                Nevek[i+1] = tmp
                tmp = Pontok[i] 
                Pontok[i] = Pontok[i+1]
                Pontok[i+1] = tmp
                tmp = Melyseg[i] 
                Melyseg[i] = Melyseg[i+1]
                Melyseg[i+1] = tmp
                csere = csere + 1
        if csere == 0:
            break
    # Most fogjuk elvegezni a cserelgetest a melyseg fuggvenyeben
    while True:
        csere = 0
        for i in range(len(Nevek) - 1):
            # Ha talalunk 2 egymas mellett levo azonos pontszamu sort akkor megnezzuk hogy melyiknel nagyobb a melyseg
            # Es ennek fuggvenyeben cseret hajtunk vegre
            if Pontok[i+1] == Pontok[i] and Melyseg[i+1] > Melyseg[i]:
                tmp = Nevek[i] 
                Nevek[i] = Nevek[i+1]
                Nevek[i+1] = tmp
                tmp = Pontok[i] 
                Pontok[i] = Pontok[i+1]
                Pontok[i+1] = tmp
                tmp = Melyseg[i] 
                Melyseg[i] = Melyseg[i+1]
                Melyseg[i+1] = tmp
                csere = csere + 1
        if csere == 0:
            break
    # Ki printeljuk mind3 tablazatot tabulatorokkal, esztetikusan
    for i in range(len(Nevek)):
        print(f"{Nevek[i]} \t {Pontok[i]} \t {Melyseg[i]}")

if __name__ == "__main__":
    main()
