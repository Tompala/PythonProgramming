def main():
    Nevek   = ['Jonas', 'Alex', 'Lajos', 'Geza', 'Hedvig', 'Ferenc', 'Margit', 'Lujza', 'Fityma', 'Stajn']
    Pontok  = [88,       88,     65,      82,     73,       79,       74,       88,      82,       78    ]
    Melyseg = [2.4177,   3.3651, 3.1516,  2.7217, 3.2465,   2.1788,   2.4110,   2.3872,  2.2805,   1.8338]
    # Ugyan az az elv, mint a kezdonel
    while True:
        csere = 0
        for i in range(len(Nevek) - 1):
            if Pontok[i+1] > Pontok[i]:
                # Itt azonban az atlathatosag es egszerusites erdekeben fuggvenyt hasznalunk
                # egy tablazat elemeinek a felcserelesere
                Nevek = ElemCsere(Nevek, i, i+1)
                Pontok = ElemCsere(Pontok, i, i+1)
                Melyseg = ElemCsere(Melyseg, i, i+1)
                csere = csere + 1
        if csere == 0:
            break
    while True:
        csere = 0
        for i in range(len(Nevek) - 1):
            if Pontok[i+1] == Pontok[i] and Melyseg[i+1] > Melyseg[i]:
                Nevek = ElemCsere(Nevek, i, i+1)
                Pontok = ElemCsere(Pontok, i, i+1)
                Melyseg = ElemCsere(Melyseg, i, i+1)
                csere = csere + 1
        if csere == 0:
            break
    
    for i in range(len(Nevek)):
        print(f"{Nevek[i]} \t {Pontok[i]} \t {Melyseg[i]}")
        
    def ElemCsere(T, i, j):
        tmp = T[i]
        T[i] =T[j]
        T[j] = tmp
        return T

if __name__ == "__main__":
    main()
