# Átadjuk a num1 változónak az első beírt számot
num1 = input("Enter the 1st number: ")
# Átadjuk a num1 változónak a második beírt számot
num2 = input("Enter the 2nd number: ")
# A num1 es num2 valtozok szoveg tipusuak es mint azt megtanultuk szovegek oszeadasa
# azok egyszeru osszevonasat jelentik. Emiat szuksegunk van ezeket a valtozokat mas
# valtozo tipussa "cast"-olni. Az "int" az "integer" vagyis "egesz szam" roviditese
# es amennyiben szovegunket ilyen modon integerbe castoljuk ugy mar matematikai muveletek
# helyesen vegezhetoek vele
eredmeny = int(num1) + int(num2)
# A szamot ezutan vissza is kell castolnunk string tipussa, hogy a print fuggveny tudja kezelni
print("Az összeadás eredménye: " + str(eredmeny))
