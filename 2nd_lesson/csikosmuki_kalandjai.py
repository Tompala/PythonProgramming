print("Csikosmuki felebredt reggel es el kellett hataroznia magat, hogy mit csinal.")

direction = input("Merre induljon Csikosmuki: eszak, kelet, del, vagy nyugat? ")

if direction.lower() == 'eszak':
    action = input("Egy to van Csikosmuki elott. Atussza-e vagy hajot epitsen? (atussza/hajo) ")
    if action.lower() == 'atussza':
        print("Csikosmuki sikeresen atuszta a tavat.")
    elif action.lower() == 'hajo':
        print("Csikosmuki epített egy hajót és átszelt a tavon.")
    else:
        print("Csikosmuki nem tud dönteni, így hazamegy.")
elif direction.lower() == 'kelet':
    action = input("Egy nagy fal allja Csikosmuki utjat. Atmasszon rajta vagy assoon alatta lyukat? (atmassza/alatt) ")
    if action.lower() == 'atmassza':
        print("Csikosmuki átmászott a falon.")
    elif action.lower() == 'alatt':
        print("Csikosmuki ásott egy lyukat és atbujt a fal alatt.")
    else:
        print("Csikosmuki nem tud dönteni, így hazamegy.")
elif direction.lower() == 'del':
    action = input("Egy sivatag van Csikosmuki elott. Megkeruli vagy nekivag? (megkerul/nekivag) ")
    if action.lower() == 'megkerul':
        print("Csikosmuki megkerülte a sivatagot.")
    elif action.lower() == 'nekivag':
        print("Csikosmuki nekivágott a sivatagnak és sikeresen átkelt.")
    else:
        print("Csikosmuki nem tud dönteni, így hazamegy.")
elif direction.lower() == 'nyugat':
    action = input("Egy hegseg van Csikosmuki elott. Atmehet barlangokon vagy megmaszhatja a hegyeket. (barlangok/maszas) ")
    if action.lower() == 'barlangok':
        print("Csikosmuki átment a barlangokon keresztül.")
    elif action.lower() == 'maszas':
        print("Csikosmuki megmászta a hegyeket.")
    else:
        print("Csikosmuki nem tud dönteni, így hazamegy.")
else:
    print("Csikosmuki otthon marad.")

print("És így végződik Csikosmuki kalandja... egyelőre.")