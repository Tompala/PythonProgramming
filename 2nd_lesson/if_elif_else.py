# Bemenetet kerunk a felhasznalotol
eso = input("Esik az eso? [1 - igen / 0 - nem]: ")
# A bolvasott string erteket atcastoljuk integer tipussa. Emlekszunk hogy a
# logikai valtozo tipus a boolian True vagy False lehet, azonban ez azonosan egyenlo
# az 1 es 0 ertekekkel, igy ha 1 es 0 egesz szamaink vannak azt hasznalhatjuk logikai
# ertekekkent.
# (A python a stringeket is kepes logikai ertekkent ertelmezni ott azonban barmilyen
# string ami tartalmaz karaktereket True erteknek felel meg es csak az ures string a False
# pl.: "0" -- True, "" -- False, igy az int-be valtas a megfelelo dontes)
eso = int(eso)
#Megegyszer a hidegre is
hideg = input("Hideg van? [1 - igen / 0 - nem]: ")
hideg = int(hideg)
# Megirljuk a felteteles utasitast
# HA esik ES hideg van
if eso and hideg:
    print("Kell a kabat es esernyo is!")
# KULONBEN HA NEM esik ES hideg van
elif not eso and hideg:
    print("Kelleni fog a kabat!")
# KULONBEN HA esik ES nincs hideg
elif eso and not hideg:
    print("Eleg ha viszel esernyot!")
# KULONBEN
else:
    print("Jo lesz csak a pulcsi!")
    