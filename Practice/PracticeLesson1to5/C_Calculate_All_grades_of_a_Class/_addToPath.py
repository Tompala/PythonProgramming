import os
import sys

def addParentFolderToPath(upper):
    folder = os.path.dirname(os.path.abspath(__file__))
    for _ in range(upper):
        folder = os.path.join(folder, '..')
    for dirpath, _ , _ in os.walk(folder):
        sys.path.append(dirpath)