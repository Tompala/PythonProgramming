from _addToPath import addParentFolderToPath
addParentFolderToPath(1)
import GradeCalculatorProgramvaz
import CalculateTheGradeOfAClassProgramvaz

def main():
    semesterScores = [
    [98, 88, 75, 52, 94, 92, 100, 85, 87, 91, 77, 85, 85, 50, 74, 86, 89, 95, 96, 93, 50, 90, 88, 84, 80, 93, 92, 96, 91, 89, 82, 97],
    [82, 79, 83, 46, 97, 92, 85, 90, 88, 95, 93, 50, 75, 84, 87, 81, 26, 80, 85, 87, 88, 92, 77, 74, 78, 86, 83, 79, 75, 77, 81, 82],
    [83, 89, 85, 81, 82, 79, 75, 77, 81, 82, 86, 88, 90, 92, 88, 86, 41, 79, 85, 86, 82, 80, 78, 75, 72, 76, 78, 79, 81, 82, 84, 87],
    [88, 91, 85, 82, 78, 53, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 54, 75, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90],
    [92, 94, 90, 85, 82, 79, 75, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 79, 75, 77, 81, 82, 86],
    [88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 79, 42, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92]
    ]

    semesterGrades = calculateAllGrades(semesterScores)
    for testgrades in semesterGrades:
        print(f"{testgrades}")
        
    completeAverage = calculateCompleteAverage(semesterGrades)
    print(f"{completeAverage}")
    
    averageByStudents = calculateAverageByStudents(semesterGrades)
    print(f"{averageByStudents}")

def calculateAllGrades(allScores):
    # ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘
    # Ide irjuk a feladatot
    # Ne feledjuk a teljes tomb osztalyzat szamolo fuggvenyunket meghivhatjuk ekeppen:
    # CalculateTheGradeOfAClass.calculateGrades()
    # persze bemenetet adnunk kell neki, hiszen azt elvarta az elozo feladatban is.
    # ^^^^^^^^^^^^^^^^^^^^^^
    pass

def calculateCompleteAverage(allGrades):
    # ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘
    # Ide irjuk a feladatot
    # Ne feledjuk a teljes tomb atlag szamolo fuggvenyunket meghivhatjuk ekeppen:
    # CalculateTheGradeOfAClass.calculateAverage()
    # persze bemenetet adnunk kell neki, hiszen azt elvarta az elozo feladatban is.
    # ^^^^^^^^^^^^^^^^^^^^^^
    pass

def calculateAverageByStudents(allGrades):
    # ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘
    # Ide irjuk a feladatot
    # Ez tenyleg nehez feladat. Nem baj ha nem sikerul megoldani.
    # ^^^^^^^^^^^^^^^^^^^^^^
    pass
    
if __name__ == "__main__":
    main()