from _addToPath import addParentFolderToPath
addParentFolderToPath(1)
import GradeCalculator
import CalculateTheGradeOfAClass

def main():
    semesterScores = [
    [98, 88, 75, 52, 94, 92, 100, 85, 87, 91, 77, 85, 85, 50, 74, 86, 89, 95, 96, 93, 50, 90, 88, 84, 80, 93, 92, 96, 91, 89, 82, 97],
    [82, 79, 83, 46, 97, 92, 85, 90, 88, 95, 93, 50, 75, 84, 87, 81, 26, 80, 85, 87, 88, 92, 77, 74, 78, 86, 83, 79, 75, 77, 81, 82],
    [83, 89, 85, 81, 82, 79, 75, 77, 81, 82, 86, 88, 90, 92, 88, 86, 41, 79, 85, 86, 82, 80, 78, 75, 72, 76, 78, 79, 81, 82, 84, 87],
    [88, 91, 85, 82, 78, 53, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 54, 75, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90],
    [92, 94, 90, 85, 82, 79, 75, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 79, 75, 77, 81, 82, 86],
    [88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92, 94, 90, 85, 82, 79, 42, 77, 81, 82, 86, 88, 85, 82, 78, 76, 80, 83, 85, 88, 90, 92]
    ]

    semesterGrades = calculateAllGrades(semesterScores)
    for testgrades in semesterGrades:
        print(f"{testgrades}")
        
    completeAverage = calculateCompleteAverage(semesterGrades)
    print(f"{completeAverage}")
    
    averageByStudents = calculateAverageByStudents(semesterGrades)
    print(f"{averageByStudents}")

def calculateAllGrades(allScores):
    # Az elozo feladathoz hasonload definialunk egy ures tombot. Ehhez fogjuk appendelni az egyes elemeket
    # Ez esetben az elemek nem szamok hanem tombok lesznek
    allGrades = []
    # A kovetkezo for ciklus minden iteraciojaban testScore valtozo egy 32 elemu tombot tartalmaz a 32 pontszammal
    # Erre az elozo feladatban megirt calculateGrades() fuggvenyt meg tudjuk hivni ami egy 32 elemu tombbel ter vissza
    # ami az osztalyzatokat tartalmazza. Ezt fogjuk hozza append-elni az allGrades tombunkhoz.
    for testScore in allScores:
        allGrades.append(CalculateTheGradeOfAClass.calculateGrades(testScore))
    # Visszaterunk allGrades-el
    return allGrades

def calculateCompleteAverage(allGrades):
    # Hasonloan jarunk el az elozo feladathoz. Ezuttal egy dolgozat atlagat mar ki tudjuk szamolni
    # az elozo feladatban definialt calculateAverage() fuggvennyel. Ezeket az atlagokat adjuk most ossze
    # majd a vegen elosztjuk a dolgozatok szamaval ami ugye a testGrades tomb hossza (ez a tomb 6 elembol all
    # ne tevesszen meg bennunket, hogy az a 6 elem darabonkent 32 elembol all, az akkor is cska 6 elem, igy 
    # allGrades tomb hossza 6)
    sumAverage = 0
    for testGrades in allGrades:
        sumAverage += CalculateTheGradeOfAClass.calculateAverage(testGrades)
    
    return sumAverage / len(allGrades)

def calculateAverageByStudents(allGrades):
    # Definialunk egy sumGrades tombot melynek hossza 32 elem lesz. Ezt nem hardcode megoldassal 
    # [0] * 32-kent definialjuk hanem [0] * len(allGrades[0])-val ahol allGrades[0] az allgrades tomb
    # elso eleme ami egy 32 elembol allo tomb. Igy len(allGrades[0]) = 32.
    sumGrades = [0] * len(allGrades[0])
    # A 2D tablazat minden elemen vegig akarunk iteralni, igy 2 egymasba agyazott for ciklusra lesz szuksegunk.
    # Az elso a sorokon fog vegigmenni, ig i 0-tol 5-ig megy. Minden i ertekre j vegig megy a tabla egy soranak
    # minden elemen, igy j 0-tol 31-ig megy. Ezt teszi 6x. igy a teljes iteraciok szama 6x32 ami pont a tablazatunk
    # osszes elemenek szama.
    for i in range(len(allGrades)):
        for j in range(len(allGrades[i])):
            # Itt a csel. j minden iteraciojara hozzaadjuk a tablazat eppen aktualis elemet sumGrades[j]-hez ami
            # sumGrades j-dik eleme. Mivel i viszont 0-tol 5-ig megy igy sumGrades minden elemehez 6x adunk hozza valamit.
            # Ez lesz minden oszlop osszes eleme, hiszen minden oszlop 6 elemet tartalmaz. Ennek az iteracionak a vegen
            # sumGrades minden eleme egy szam amit el kell osztanunk 6-al, hogy az atlagot megkapjuk. Ezt megtesszuk, de
            # nem csak siman 6-al osztunk hanem ezt parameteresen tesszuk, hogyha a tablazatunkat atirjuk 7 sorosra akkor is
            # jo legyen aprogramunk
            sumGrades[j] += allGrades[i][j]
    
    # Definialunk megegy 32 elemu tombot majd egy for ciklussal az elozoleg kapott tomb minden elemen vegigiteralunk.
    # Minden elemet elosztunk a tesztek szamaval ami len(allGrades) esetunkben 6, es ezt a szamot egyenlove teszzuk a
    # most letrehozott averageGrades ugyan azon indexu elemevel. Ez utan mar csak vissza kell terni a kapott tombbel
    averageGrades = [0] * len(sumGrades)
    for i in range(len(sumGrades)):
        averageGrades[i] = sumGrades[i] / len(allGrades)
        
    return averageGrades
    
if __name__ == "__main__":
    main()