from _addToPath import addParentFolderToPath
addParentFolderToPath(1)
import GradeCalculatorProgramvaz

def main():
    classScores = [98, 88, 75, 52, 94, 92, 100, 85, 87, 91, 77, 85, 85, 50, 74, 86,
                   89, 95, 96, 93, 50, 90, 88, 84, 80, 93, 92, 96, 91, 89, 82, 97]
    classGrades = calculateGrades(classScores)
    print(f"The grades of the class are: {classGrades}")
    average = calculateAverage(classGrades)
    print(f"The average of the class is {average}")

def calculateGrades(scores):
    # ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘
    # Ide irjuk a feladatot
    # Ne feledjuk az osztalyzat szamolo fuggvenyunket meghivhatjuk ekeppen:
    # GradeCalculatorProgramvaz.calculateGrade()
    # persze bemenetet adnunk kell neki, hiszen azt elvarta az elozo feladatban is.
    # ^^^^^^^^^^^^^^^^^^^^^^
    pass

def calculateAverage(grades):
    # ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘
    # Ide irjuk a feladatot
    # ^^^^^^^^^^^^^^^^^^^^^^
    pass
    
if __name__ == "__main__":
    main()