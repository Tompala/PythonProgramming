from _addToPath import addParentFolderToPath
addParentFolderToPath(1)
import GradeCalculator

def main():
    classScores = [98, 88, 75, 52, 94, 92, 100, 85, 87, 91, 77, 85, 85, 50, 74, 86,
                   89, 95, 96, 93, 50, 90, 88, 84, 80, 93, 92, 96, 91, 89, 82, 97]
    classGrades = calculateGrades(classScores)
    print(f"The grades of the class are: {classGrades}")
    average = calculateAverage(classGrades)
    print(f"The average of the class is {average}")

def calculateGrades(scores):
    # Definialunk egy ures tombot, hogy a grades.append() metodussal hozza tudjunk adogatni elemeket
    grades = []
    # A kovetkezo for ciklus minden iteraciojaban score valtozo tartalmazza az egyes pontszamokat
    # Minden pontszamra az elozo feladatban definialt osztalyzat szamolo fuggvenyt meghicjuk es amit
    # visszakapunk azt betesszuk a grades tombbe.
    for score in scores:
        grades.append(GradeCalculator.calculateGrade(score))
    # visszaterunk grades tombbel
    return grades

def calculateAverage(grades):
    # Ilyet mar csinaltunk. Egy sum valtozoban gyujtjuk az osztalyzatokat mindig az eddigiekhez hozzaadva
    # a kovetkezot, majd a vegen a teljes osszeget elosztjuk az osztalyzatok szamaval, megkapva az atlagot
    sum = 0
    for grade in grades:
        sum += grade
    # len(grades) az osztalyzatok szama, hiszen ez a grades tomb hosszat jelenti
    return sum / len(grades)
    
if __name__ == "__main__":
    main()