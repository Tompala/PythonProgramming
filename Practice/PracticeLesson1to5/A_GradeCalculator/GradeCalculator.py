def main():
    score = float(input("What was the score the student achieved?: "))
    print(f"The student got a >> {calculateGrade(score)} << for the test!")
    
def calculateGrade(score):
    # A grade valtozot azert definialjuk az if-else elott, mert igy rovidebb a kodunk.
    # Nem kell leirnunk, hogy: 
    # if score < 50 :
    # Emellett a fontosabb megfontolas, hogy azt a valtozot amivel visszaterunk egy fuggvenybol
    # mindig a fuggveny legelejen definialjuk, nehogy elofordulhasson, hogy esetleg nem lesz 
    # definialva a fuggvenny lefutrasa soran es igy egy nemletezo valtozot probalnank a fuggveny vegen visszaadni.
    grade = 1
    if score >= 50 and score < 65 :
        grade = 2
    elif score >= 65 and score < 75:
        grade = 3
    elif score >= 75 and score < 90:
        grade = 4
    elif score >= 90:
        grade = 5
        
    return grade
    
if __name__ == "__main__":
    main()